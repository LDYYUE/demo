package com.huiLiangJinChen;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class SocketDemo {
    public static void main(String[] args) {
        createSocket();
    }
    /**
     * 建立socket连接
     */
    public static void createSocket(){
        ServerSocket serverSocket = null;
        Socket socket = null;
        BufferedReader readHttp = null;
        String httpRequestHeader;
        try {
            serverSocket = new ServerSocket(80);
            while(true){
                socket = serverSocket.accept();
                readHttp = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                httpRequestHeader = readHttp.readLine();
                OutputStream out = socket.getOutputStream();
                StringBuilder response = new StringBuilder();
                System.out.println(httpRequestHeader);
                //再写个函数，判断读取到的url中是要求加还是乘getOperateMethod（String httpRequestHeader）
                //再来个获取要操作的数getOperateNum(Sring httpRequestHeader)
                String operateMethod = getOperateMethod(httpRequestHeader);
                int num1 = Integer.valueOf(getOperateNum(httpRequestHeader).get(0).toString());
                int num2 = Integer.valueOf(getOperateNum(httpRequestHeader).get(1).toString());
                System.out.println(getOperateNum(httpRequestHeader));
                switch (operateMethod){
                    case "ad":
                        //将两个数相加
                        response.append("HTTP/1.1 200 OK\r\n");
                        response.append("Content-type:text/html\r\n\r\n");
                        int sum = num1+num2;
                        response.append("sum is: ").append(sum);
                        out.write(response.toString().getBytes(StandardCharsets.UTF_8));
                        out.close();
                        socket.close();
                        serverSocket.close();
                        break;
                    case "mu":
                        //两数相乘
                        response.append("HTTP/1.1 200 OK\r\n");
                        response.append("Content-type:text/html\r\n\r\n");
                        int multi = num1*num2;
                        response.append("result: ").append(multi);
                        out.write(response.toString().getBytes(StandardCharsets.UTF_8));
                        out.close();
                        socket.close();
                        serverSocket.close();

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取操作方式
     * @param str
     * @return
     */
    public static String getOperateMethod(String str){
        int strlen = str.length();
        StringBuilder result = new StringBuilder();
        for(int i=0;i<strlen;i++){
            if(str.charAt(i)=='/' && i+2<strlen){
                result.append(str.charAt(i + 1));
                result.append(str.charAt(i + 2));
                return result.toString();
            }
        }
        return "err";
    }
    /**
     * 获取操作数
     * @return
     */
    public static List<StringBuilder> getOperateNum(String str){
        int strlen = str.length();
        List<StringBuilder> nums = new ArrayList<>();
        for(int i=0;i<strlen;i++){
            if(str.charAt(i)=='='){
                StringBuilder temp = new StringBuilder();
                i++;
                while (str.charAt(i)!='&' && str.charAt(i)!=' '){
                    temp.append(str.charAt(i));
                    i++;
                }
                nums.add(temp);
            }
        }
        return nums;
    }
}
